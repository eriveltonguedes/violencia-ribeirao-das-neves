# Combate à Violência em Ribeirão das Neves - MG

URS	Belo Horizonte
Município	Ribeirão das Neves
Referência Técnica municipal	Antonela Murari
Referência Técnica URS	
Data	09/11/2021
PLANO DE AÇÃO 
Resolução CIB-SUS 77.32 de 22de setembro de 2021
https://www.saude.mg.gov.br/index.php?option=com_gmg&controller=document&id=22728-resolucao-cib-sus-mg-n-7-732-de-22-de-setembro-de-2021&task=download

Institui o repasse de incentivo financeiro, em caráter excepcional, para fortalecimento da Vigilância das Causas Externas (Violências e Acidentes de Trânsito) em Minas Gerais.

Eixo	
Atividade	Meta	
Interfaces/ Parceiros	
Prazo	
Responsável	
Valor Estimado(R$)
Vigilância das Violências	Capacitar os profissionais quanto ao preenchimento da ficha de notificação de
violência interpessoal/autoprovocada.	70% da rede capacitada	Núcleo de Epidemiologia Hospitalar do HSJT	11 meses a partir do recebimento do recurso	Gerência de Epidemiológia	Aquisição dos seguintes aparelhos:
- 03 Notebooks – R$20.970,00
- 01 datashow – R$4.500,00
- 06 web cam – R$1.200,00
- Lanche R$2.400,00
- 06 ventiladores de teto: R$3.000,00
- Reforma do espaço de convivência da Secretaria de Saúde para as reuniões e capacitações sobre violência: R$50.252,74
 

	Ampliar as unidades notificadoras de violência.	Mínimo 1 unidade	Unidades assistencais que compõem a Superintendência de Assistência e Promoção à Saúde	12 meses a partir do recebimento do recurso	Núcleo de Promoção a Saúde e Cultura da Paz	Veiculo para visitar as unidades de saúde para sensibilização e suporte a equipe para a realização das outras atividades deste Plano.
- Aluguel de veículo: R$58.980,00 (para 12 meses)
- Combustivel: R$ 18.000 (para 12 meses)
	Qualificar os dados inseridos    na ficha de investigação de intoxicação exógena, quanto às tentativas de suicídio e de violência interpessoal/
autoprovocada.	90% das fichas do sinan	Unidades assistencais que compõem a Superintendência de Assistência e Promoção à Saúde	11 meses a partir do recebimento do recurso	Gerência de Epidemiológia e Núcleo de Promoção a Saúde e Cultura da Paz	 Melhorar as condições de trabalho dos servidores responsaveis por esta ação com a aquisição dos seguintes itens.
- 04 mesas para computador com 2 gavetas – R$3.409,32
- 06 cadeiras – R$1.860,00
- 03 computadores - R$20.697,00
- 04 armários – R$7.319,32

	Verificar se as fichas de investigação de intoxicação exógena, quanto às tentativas de suicídio, estão notificadas
na ficha de violência interpessoal/autoprovocada.	80% das fichas do SINAN	Unidades assistencais que compõem a Superintendência de Assistência e Promoção à Saúde	11 meses a partir do recebimento do recurso	Gerência de Epidemiológia e Núcleo de Promoção a Saúde e Cultura da Paz	Impressão de 1.000 fichas de notificação de violência e 1.000 fichas de notificação intoxicação exogena: R$360,00.
	Elaborar boletim de vigilância  das violências e divulgá-lo.	1 Boletim	Gerência de Epidemiológia e Núcleo de Promoção a Saúde e Cultura da Paz.	Fevereiro de 2022.	Núcleo de Geoinformação em Saúde 	Tiragem de 1000 boletins epidemiológicos para distribuição: R$5.500,00. 
Violência Sexual	Capacitar os profissionais dos  Hospitais de referência no atendimento a pessoa em situação de violência sexual quanto ao preenchimento da ficha de notificação de violência interpessoal/
autoprovocada.	100% do hospitais de referência	HSJT	11 meses a partir do recebimento do recurso.	Núcleo de Epidemiologia do HSJT, Gerência de Epidemiologia e Núcle de Promoção a Saúde e Cultura da Paz.	Aquisição de equipamentos para educação continuada do Núcleo de Epidemiologia Hospitalar e Saúde da Mulher.
- 01 Notebook – R$6.990,00
- 01 datashow – R$4.500,00
- 02 web cam – R$400,00
- Tela de projeção: R$650,00
- 02 Computadores - R$ 13.798,00
- 02 mesas - R$1.704,00
- 02 armário - R$3.659,66
 

						- 05 cadeiras – R$1.550,00
- 02 ventiladores de parede: R$614,8
- Lanche: R$1.000,00
- 1000 squizes: R$13.500,00
- Material gráfico: R$3.000,00
- Banners: R$300,00
	Estruturar o fluxo de atendimento a pessoa em situação de violência sexual, a partir de pactuações realizadas por parceiros intra e intersetorias, e divulgá-lo
para serviços de saúde, parceiros e sociedade civil.	1 Fluxo sistematizado	Revim	11 meses a partir do recebimento do recurso.	Gerência de Epidemiologia e Núcleo de Promoção e Cultura da Paz.	Impressão de 300 tiragens do fluxo para dvulgação: R$270,00.
Cuidado Integral	Mapear a rede de enfrentamento à violência no município.	100% da rede mapeada.	REVIM	11 meses apartir do recebimento do recurso	Núcleo de Promoção e Cultura da Paz	
	Conhecer a competência de    cada ator da rede de enfrentamento à violência e  socializar os conhecimentos
com os demais profissionais.	2 encontros realizados	REVIM/ Superintendência de Assistência e Promoção à Saúde, Superintendência Hospital e de Urgência	11 meses a partir do recebimento do recurso.	Núcleo de Promoção e Cultura da Paz	Confecção da e divulgação de 3.400 cartilhas informando os serviços para encaminhamento R$17.000,00.
Aquisição de móveis de para melhorar estrutura de trabalho dos profissionais do Núcleo de Promoção à Saúde;
- 02 mesas: R$1.704,66
- 04 cadeiras: R$1.240,00
- 02 ventiladores de parede: R$614,8
Realizar workshop e ciclo de palestra com os entes envolvidos para rede de saúde do municipio, para isso é necessário:
- 1300 blocos de anotação: R$16.250.
- Jogos educativos para dinâmica: R$998,00.
- Contratação de especialista para explanar sobre os assuntos ( Violência infantil, violência contra o idoso, violência contra a mulher e violência sexual, suicidio) – R$25.000,00.
- Lanche: R$5.000,00
 

Mobilização Social/ Educação em saúde
	Realizar, pelo menos, duas campanhas de prevenção relacionadas à violência.	2 campanhas	Superintendência de Assistência e Promoção à Saúde, Superintendência Hospital e de Urgência, 	11 meses a partir do recebimento do recurso.	Núcleo de Promoção e Cultura da Paz	- Aquisição de 01 quadro branco 120 x 250 cm R$541,00.
Confecção dos seguintes materiais gráficos para divulgação aos profissionais de saúde:
- 1.900 calendários de mesa – R$ 16.910,00
- 700 blusas distribuidas entre os temas das campanhas de prevenção relacionadas a violência – R$15.400,00
- 3.000 Mochilas saco: R$15.000,00.
	Qualificar os profissionais de  saúde quanto à temática violência.	2 capacitações	Superintendência de Assistência e Promoção à Saúde, Superintendência Hospital e de Urgência,	11 meses a partir do recebimento do recurso.	Núcleo de Promoção e Cultura da Paz	Aquisição de materiais gráficos:
- 1.000 blocos de anotações 200 mm x 273 mm com o tema relacionado as violências – R$12.500,00.
- 1.000 sacolas retornáveis com o tema violência R$7.980,00.
- Lanche:R$2.400,00 

Vigilância dos Acidentes de Trânsito	Identificar as fontes de informação existentes sobre acidentes de trânsito.	1 encontro	40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
  SMS
	Até maio de 2022.	Núcleo de Geoinfomação em Saúde/SMSA e Gerência de     Educação para o Trânsito/SMST	
	Realizar análise dos dados, com as informações sobre as vítimas dos acidentes de trânsito e outras características das vítimas e dos locais de ocorrência.	2 encontros	40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
  SMS	Até maio de 2022.	Núcleo de Geoinfomação em Saúde/SMSA e Gerência de     Educação para o Trânsito/SMST	Aquisição de móveis de para melhorar estrutura de trabalho dos profissionais do Núcle de Geoinformação em Saúde
01 mesa: R$852,33
02 cadeiras com rodinhas: R$1.988,00
01 armaário baixo: R$1.922,33
01 armário médio: R$1.619,83
Participação de 4 servidores em cursos de:   - Estatistica “Linguagem em R”- R$4.200,00
Power-BI: R$1.400,00
3 cadeiras: R$930,00
	Elaborar o diagnóstico situacional sobre acidentes de trânsito.	1 diagnóstico	 40º BPM-MG
  SMST	Até junho de 2022.
	Núcleo de Geoinfomação em Saúde/SMSA e Gerência de     Educação para o Trânsito/SMST	02 Quadros branco 120 cmx 200cm: R$885,34
Tiragem e distribuição de 10 documentos do “Diagnóstico situacional sobre acidentes de trânsito”- R$225,00.

	
Elaborar boletim de vigilância  das acidentes de trânsito e divulgá-lo.	01 Boletim Epidemiológico 	 
40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
  SMS	Até junho de 2022.	Núcleo de Geoinfomação em Saúde/SMSA e Gerência de     Educação para o Trânsito/SMST.	Tiragem de 1000 boletins epidemiológicos para distribuição: R$5.500,00.
Articulação interinstitucional	Identificar órgãos e parceiros que trabalham com a temática  trânsito.	01 encontro	 40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
  SMS	11 meses a partir da data de recebimento do recurso.	Núcleo de Promoção a Saúde e Cultura da Paz/SEMSA e Gerência de         Educação para o Trânsito/SMST	 

	Conhecer a competência de cada órgão/parceiro que trabalha com temática trânsito e socializar os conhecimentos com os demais profissionais.	02 encontros	40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
	 11 meses a partir da data de recebimento do recurso	Núcleo de Promoção a Saúde e Cultura da Paz/SEMSA e Gerência de         Educação para o Trânsito/SMST 	Realizar 2 wokshops 
- Material de escritório: R$2.000,00
- Papelaria: R$1.500,00
- 1300 blocos de anotação: R$16.250.
- Contratação de especialista para explanar sobre os assuntos ( Violência no trânsito)
- Lanche: R$5.000,00
Cuidado integral	Propor estratégias de intervenções no âmbito da promoção da saúde e educação à saúde visando a  redução das lesões causadas pelo trânsito.	02 ações	 40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
Sociedade civil
	 11 meses a partir da data de recebimento do recurso	Núcleo de Promoção a Saúde e Cultura da Paz/SEMSA e Gerência de         Educação para o Trânsito/SMST	- Materiais publicitários como banner, outdoor:R$15078,05
- 02 computadores:13.798
- 02 mesas: R$1.704,66
-02 cadeiras: R$310,00
	Propor estratégias de intervenções de prevenção dos acidentes de trânsito a partir dos fatores e condutas de risco identificados no diagnóstico situacional.	02 ações	  40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
  SMS	 11 meses a partir da data de recebimento do recurso	Núcleo de Promoção a Saúde e Cultura da Paz/SEMSA e Gerência de         Educação para o Trânsito/SMST	3 Tendas: R$1500,00
 

Mobilização Social/ Educação em saúde	Realizar, pelo menos, duas campanhas de prevenção aos acidentes de trânsito.	02 campanhas	  40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
	11 meses a partir da data de recebimento do recurso	Núcleo de Promoção a Saúde e Cultura da Paz/SEMSA e Gerência de         Educação para o Trânsito/SMST	-Aquisição de prêmios para concusos de redação nas escolas com as temáticas que se referem a trânsito – R$20.678,05
	Qualificar a equipe técnica que trabalha com a temática   trânsito.	02 capacitações	  40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
  SESTSENAT
	11 meses a partir da data de recebimento do recurso	 Núcleo de Promoção a Saúde e Cultura da Paz/SEMSA e Gerência de         Educação para o Trânsito/SMST	Contratação de curso para os profissionais com a temática trânsito: R$50.000,00.
	Realizar atividades educativas e de promoção da saúde para prevenção aos acidentes de trânsito.	02 ações	40º BPM-MG
  Polícia Civil
  PRF
  SEJUSP-MG
	 11 meses a partir da data de recebimento do recurso	Núcleo de Promoção a Saúde e Cultura da Paz/SEMSA e Gerência de         Educação para o Trânsito/SMST	Confecção de materiais educativos:
- 700 Blusas: R$15.400,00 blusas;
- Antenas para motos: R$13.500,00
- Viseiras: R$14.800,00
- Mochilas saco: R$15.000,00
- Outdoors nas regiões do Centro, Veneza e Justinopólis: R$7.000,00


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/eriveltonguedes/violencia-ribeirao-das-neves.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/eriveltonguedes/violencia-ribeirao-das-neves/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
