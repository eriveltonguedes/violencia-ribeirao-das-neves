from qgis.core import *
from qgis.core import QgsProject
from PyQt5.QtCore import QFileInfo
from qgis.core import QgsVectorLayer, QgsDataSourceUri
from qgis.utils import *

# Get the project instance
project = QgsProject.instance()
project.clear() #Close project1


#identify crs
crs = QgsCoordinateReferenceSystem(4674, QgsCoordinateReferenceSystem.EpsgCrsId)

# Print the current project file name (might be empty in case no projects have been loaded)
print(project.fileName())

host10="host10"
port="5432"
db_name= "db_name"
db_user= "db_user"
db_pass= "db_pass"

schem_name1 = "spat"
tablename1 = ["ed_territorios_paises"]
geometrycol1 = "edterritorios_geometry"

schem_name2 = "spat"
tablename2 = ["ed_territorios_paises","ed_territorios_regioes","ed_territorios_uf","ed_territorios_municipios"]
geometrycol2 = "edterritorios_geometry"

uri = QgsDataSourceUri()
# set host name, port, database name, username and password
uri.setConnection(host10, port, db_name, db_user, db_pass)
# set database schema, table name, geometry column and optionally
# subset (WHERE clause)

canvas = iface.mapCanvas()

# Disable mousewheel if you want
# QSettings().setValue("/qgis/zoom_factor", 1)

#minScale = 10000
#maxScale = 90000000

#tablename= tablename1
#uri.setDataSource(schem_name1, tablename1, geometrycol1)
# , "cityid = 2643", "primary_key_field")
#vlayer = QgsVectorLayer(uri.uri(False), "World2", "postgres")
#print("Layer valido: "+str(vlayer.isValid()))
#QgsProject.instance().addMapLayer(vlayer)

tablename= tablename2[0]
uri.setDataSource(schem_name2, tablename, geometrycol2) # , "cityid = 2643", "primary_key_field")
vlayer = QgsVectorLayer(uri.uri(False), "World", "postgres")
print("Layer valido: "+str(vlayer.isValid()))
QgsProject.instance().addMapLayer(vlayer)

# World
#Then we set the minimum and maximum map scales at which the layer is visible:
vlayer.setScaleBasedVisibility(True)
vlayer.setMinimumScale(2000000000)
vlayer.setMaximumScale(100000)
opacity = 0.5
vlayer.setOpacity(opacity)


tablename= tablename2[1]
uri.setDataSource(schem_name2, tablename2[1], geometrycol2) # , "cityid = 2643", "primary_key_field")
vlayer = QgsVectorLayer(uri.uri(False), "Regioes", "postgres")
#vlayer.scaleBasedVisibility(true)
print("Layer valido: "+str(vlayer.isValid()))
QgsProject.instance().addMapLayer(vlayer)
vlayer.renderer().symbol().setColor(QColor("red"))
opacity = 0.5
vlayer.setOpacity(opacity)


#Then we set the minimum and maximum map scales at which the layer is visible:
vlayer.setScaleBasedVisibility(True)
vlayer.setMinimumScale(50000000)
vlayer.setMaximumScale(1000000)

tablename= tablename2[2]
uri.setDataSource(schem_name2, tablename, geometrycol2) # , "cityid = 2643", "primary_key_field")
vlayer = QgsVectorLayer(uri.uri(False), "UF", "postgres")
#vlayer.scaleBasedVisibility(true)
print("Layer valido: "+str(vlayer.isValid()))
QgsProject.instance().addMapLayer(vlayer)

#Then we set the minimum and maximum map scales at which the layer is visible:
vlayer.setScaleBasedVisibility(True)
vlayer.setMinimumScale(21000000)
vlayer.setMaximumScale(10000)

# create a new symbol
#symbol = QgsLineSymbol.createSimple({'line_style': 'dash', 'color': 'red'})
vlayer.renderer().symbol().setColor(QColor("yellow"))
opacity = 0.5
vlayer.setOpacity(opacity)
# apply symbol to layer renderer
#vlayer.renderer().setSymbol(symbol)

# repaint the layer
vlayer.triggerRepaint()

tablename= tablename2[3]
uri.setDataSource(schem_name2, tablename, geometrycol2) # , "cityid = 2643", "primary_key_field")
vlayer = QgsVectorLayer(uri.uri(False), "Municipios", "postgres")
print("Layer valido: "+str(vlayer.isValid()))

vlayer.setOpacity(0.5)
#lyr=vlayer
#Next we toggle scale-based visibility:
vlayer.setScaleBasedVisibility(True)

# city
#Then we set the minimum and maximum map scales at which the layer is visible:
vlayer.setMinimumScale(2000000)
vlayer.setMaximumScale(10000)
vlayer.renderer().symbol().setColor(QColor("blue"))
vlayer.setOpacity(0.5)
vlayer.triggerRepaint()

#Now we add the layer to the map:
QgsProject.instance().addMapLayer(vlayer)
#QgsMapLayerRegistry.instance().addMapLayer(vlayer)

print(vlayer.name, vlayer.maximumScale(), vlayer.hasScaleBasedVisibility )

tablename= tablename2[3]
uri.setDataSource(schem_name2, tablename, 'edterritorios_centroide') 
vlayer = QgsVectorLayer(uri.uri(False), "Capitais", "postgres")
print("Layer valido: "+str(vlayer.isValid()))

vlayer.setOpacity(0.5)
#lyr=vlayer
#Next we toggle scale-based visibility:
vlayer.setScaleBasedVisibility(True)

# capitais
#Then we set the minimum and maximum map scales at which the layer is visible:
vlayer.setMinimumScale(100000000)
vlayer.setMaximumScale(10000)
vlayer.renderer().symbol().setColor(QColor("blue"))
vlayer.setOpacity(0.5)

## capitais
pal_layer = QgsPalLayerSettings()
text_format = QgsTextFormat()
text_format.setFont(QFont("Arial", 8))
text_format.setSize(8)
pal_layer.setFormat(text_format)
pal_layer.fieldName = "edterritorios_nome"
pal_layer.enabled = True
pal_layer.placement = QgsPalLayerSettings.Line
labels = QgsVectorLayerSimpleLabeling(pal_layer)
vlayer.setLabeling(labels)
vlayer.setLabelsEnabled(True)
vlayer.triggerRepaint()

vlayer.triggerRepaint()

vlayer.setSubsetString("edterritorios_capital_uf = '1'")

if not vlayer.isValid():
    print("Layer failed to load!")
else:
    QgsProject.instance().addMapLayer(vlayer)
print (tablename2[0])
print (tablename2)
print("DDDDDD")

#
def run_script(iface):
   uri = QgsDataSourceUri()
   uri.setConnection(host10, port, db_name, db_user, db_pass)
   #uri.setDataSource(schem_name1, tablename1, geometrycol1)
   uri.setDataSource(schem_name2, tablename, geometrycol2)

   if not vlayer.isValid():
       print("Layer %s did not load" %vlayer.name())
   QgsProject.instance().addMapLayer(vlayer)

#box = QgsRectangle(xmin,ymin,xmax,ymax)
box = QgsRectangle(-74,-33,-34,6)   # Brasil
iface.mapCanvas().setExtent(box)
iface.mapCanvas().refresh()


ext = vlayer.extent()
#and there extract the Values with:

xmin = ext.xMinimum()
xmax = ext.xMaximum()
ymin = ext.yMinimum()
ymax = ext.yMaximum()
#coords = "%f,%f,%f,%f" %(xmin, xmax, ymin, ymax) # this is a string that stores the coordinates
run_script(iface)














