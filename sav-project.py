# If you are not inside a QGIS console you first need to import
# qgis and PyQt classes you will use in this script as shown below:
from qgis.core import QgsProject
# Get the project instance
project = QgsProject.instance()
# Print the current project file name (might be empty in case no projects have been loaded)
print(project.fileName())
'/home/user/projects/my_qgis_project.qgs'
# Load another project
project.read('C:/Users/r1439546/Documents/aly2.qgz')
project.read('C:/Users/r1439546/Documents/forest.qgz')
project.read('C:/Users/r1439546/Documents/comarcas.qgz')
print(project.fileName())
'/home/user/projects/my_other_qgis_project.qgs'

# Save the project to the same
project.write()
# ... or to a new file
# project.write('C:/Users/r1439546/Documents/comarcas2.qgz')