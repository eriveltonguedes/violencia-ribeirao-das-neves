from qgis.gui import *
# from qgis.PyQt.QtGui import QAction
# from qgis.PyQt.QtCore import SIGNAL, Qt, QString
from qgis.PyQt.QtWidgets import QMainWindow


#   python -m pip install PyQGIS

# If you are not inside a QGIS console you first need to import
# qgis and PyQt classes you will use in this script as shown below:
import os # This is is needed in the pyqgis console also
from qgis.core import (QgsVectorLayer)
from qgis.core import QgsProject
# Get the project instance
project = QgsProject.instance()
project.clear() #Close project1

# Print the current project file name (might be empty in case no projects have been loaded)
print(project.fileName())

#get the path to a geopackage  e.g. /home/project/data/data.gpkg
## get the path to a geopackage  e.g. /home/project/data/data.gpkg
path_to_gpkg = os.path.join(QgsProject.instance().homePath(), "data", "data.gpkg")
gis_path="P:/Apoio/GIS DataBase/"
aly_path = "P:/Apoio/GisDataBase/Aly-hidrologia.gpkg"

natural_earth_path="P:/Apoio/GisDataBase/natural_earth_vector/natural_earth_vector.gpkg"
uri = natural_earth_path + "|layername=ne_10m_admin_0_countries"
iface.addVectorLayer(uri, "countries", "ogr")
# append the layername part
places_layer = natural_earth_path + "|layername=places"
municipios_layer=aly_path + "|layername=municipios2013" 
bacias2_layer=aly_path + "|layername=Bacias_Hidrográficas_Ottocodificadas Nível 2" 

iface.addVectorLayer(bacias2_layer, ".", "ogr")

iface.addVectorLayer(municipios_layer, ".", "ogr")

canvas.setCanvasColor(Qt.white)
canvas.enableAntiAliasing(True)

canvas = QgsMapCanvas()
# canvas.show()
# set extent to the extent of our layer

#canvas.setExtent(municipios2013.extent())

print(aly_path)


from functools import partial
from qgis.core import (QgsTaskManager, QgsMessageLog,
QgsProcessingAlgRunnerTask, QgsApplication,
QgsProcessingContext, QgsProcessingFeedback,
QgsProject)
MESSAGE_CATEGORY = 'AlgRunnerTask'

layer = iface.activeLayer()
print(layer)

#get the current Layer

layer = iface.activeLayer()
#get the extent which is a QgsRectangle object

ext = layer.extent()
#and there extract the Values with:


print(ymin)

xmin = ext.xMinimum()
xmax = ext.xMaximum()
ymin = ext.yMinimum()
ymax = ext.yMaximum()
coords = "%f,%f,%f,%f" %(xmin, xmax, ymin, ymax) # this is a string that stores the coordinates

#print (coords)

box = QgsRectangle(xmin,ymin,xmax,ymax)
box = QgsRectangle(-74,-33,-34,6)   # Brasil
iface.mapCanvas().setExtent(box)
iface.mapCanvas().refresh()
