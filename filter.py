# this is how we can filter our countries vector layer to only features where the ADMIN value starts with an A:
vlayer.setSubsetString("ADMIN LIKE 'A%'")
for feature in vlayer.getFeatures():
    print(feature["ADMIN"])
  
#To remove the filter, we set an empty subset string:  
vlayer.setSubsetString("")
for feature in vlayer.getFeatures():
    print(feature["ADMIN"])
    
my_char = "B"
vlayer.setSubsetString("ADMIN LIKE '"+my_char+"%'")
print("The following country names start with {}:".format(my_char))
for feature in vlayer.getFeatures():
    print(feature['ADMIN'])
    
for feature in vlayer.getFeatures():
    print("{pop:.2f} mio people live in {name}".format(name=feature['ADMIN'],pop=feature['POP_EST']/1000000))
