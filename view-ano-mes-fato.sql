-- !preview conn=DBI::dbConnect(RSQLite::SQLite())

--código para ler ano e mês a partir da data. O ano e mês que já estão na tabela permanecem, 
--para compararmos, mas o ideal é, futuramnete, excluir tais campos.

SELECT id_ocorrencia, id_tipo_crime, id_tipo_acidente, numero_reds, data_fato,
extract(year from data_fato) as ano, extract(month from data_fato) as mes
,mes_fato, ano_fato, hora_fato, dia_sem_fato, desc_naturesa_fato, causa_presumida, codicao_climatica, id_sinalizacao
FROM public.tbl_ocorrencia;